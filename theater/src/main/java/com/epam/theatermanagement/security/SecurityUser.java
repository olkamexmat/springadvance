package com.epam.theatermanagement.security;

import com.epam.theatermanagement.domain.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;


public class SecurityUser extends User implements UserDetails {
    private static final long serialVersionUID = 1L;

    private static final String ROLE_PREFIX = "ROLE_";

    public SecurityUser(User user) {
        if (user != null) {
            this.setId(user.getId());
            this.setFirstName(user.getFirstName());
            this.setLastName(user.getLastName());
            this.setEmail(user.getEmail());
            this.setBirthday(user.getBirthday());
            this.setPassword(user.getPassword());
            this.setRole(user.getRole());
        }

    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {

        String[] roles = this.getRole().split(",");

        List<String> userRoles = Arrays.asList(roles);

        Set<GrantedAuthority> authorities = new HashSet<>();

        if (userRoles != null) {
            userRoles.forEach(role -> authorities.add(new SimpleGrantedAuthority(ROLE_PREFIX + role)));
        }

        return authorities;
    }

    @Override
    public String getUsername() {
        return super.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
