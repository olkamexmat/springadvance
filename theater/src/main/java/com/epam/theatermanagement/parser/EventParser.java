package com.epam.theatermanagement.parser;

import com.epam.theatermanagement.domain.Event;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EventParser {
	public static List<Event> getEvents(String eventData) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		List<Event> events = new ArrayList<>();
		if (!eventData.isEmpty()) {
			events = objectMapper.readValue(eventData,
					objectMapper.getTypeFactory().constructCollectionType(List.class, Event.class));
		}
		return events;
	}
}
