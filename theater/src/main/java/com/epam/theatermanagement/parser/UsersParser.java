package com.epam.theatermanagement.parser;

import com.epam.theatermanagement.domain.User;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UsersParser {

	public static List<User> getUsers(String data) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		List<User> users = new ArrayList<>();
		if (!data.isEmpty()) {
			users = objectMapper.readValue(data,
					TypeFactory.defaultInstance().constructCollectionType(List.class, User.class));
		}
		return users;
	}
}
