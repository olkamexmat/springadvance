package com.epam.theatermanagement.config;

import com.epam.theatermanagement.service.AuditoriumService;
import com.epam.theatermanagement.service.BookingService;
import com.epam.theatermanagement.service.EventService;
import com.epam.theatermanagement.service.UserAccountService;
import com.epam.theatermanagement.service.UserService;
import com.epam.theatermanagement.service.impl.AuditoriumServiceImpl;
import com.epam.theatermanagement.service.impl.BookingServiceImpl;
import com.epam.theatermanagement.service.impl.EventServiceImpl;
import com.epam.theatermanagement.service.impl.UserAccountServiceImpl;
import com.epam.theatermanagement.service.impl.UserServiceImpl;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {

    @Bean(name = "auditoriumService")
    public AuditoriumService auditoriumService() {
        return new AuditoriumServiceImpl();
    }

    @Bean(name = "eventService")
    public EventService eventService() {
        return new EventServiceImpl();
    }

    @Bean(name = "bookingService")
    public BookingService bookingService() {
        return new BookingServiceImpl();
    }

    @Bean(name = "userService")
    public UserService userService() {
        return new UserServiceImpl();
    }
    
    @Bean(name = "userAccountService")
    public UserAccountService userAccountService() {
    	return new UserAccountServiceImpl();
    }
}
