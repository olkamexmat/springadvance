package com.epam.theatermanagement.aspect;

import com.epam.theatermanagement.dao.CounterDAO;
import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.Ticket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Aspect
public class CounterAspect {
    private static final Logger LOGGER = LogManager.getLogger(CounterAspect.class);

    @Autowired
    private CounterDAO counterDAO;

    @Pointcut("execution(public com.epam.theatermanagement.domain.Event *.getByName(*))")
    private void getEventByName() {
    }

    @AfterReturning(pointcut = "getEventByName()", returning = "result")
    public void countAccess(Object result) {
        final String name = "getEventByName";
        Event event = (Event) result;
        int counter = counterDAO.getValue(name);
        counter++;
        counterDAO.update(event, name, counter);
        LOGGER.info("worked counter aspect : access event byName");
    }

    @Pointcut("execution(public double com.epam.theatermanagement.domain.Event.getBasePrice())")
    private void priceQueried() {
    }

    @After("priceQueried()")
    public void eventPriceWereQueried(JoinPoint point) {
        final String name = "priceQueried";
        Event event = (Event) point.getThis();
        int counter= counterDAO.getValue(name);
        counter++;
        counterDAO.update(event, name, counter);
        LOGGER.info("worked counter aspect : price of event was queried");
    }

    @Pointcut("execution(public void com.epam.theatermanagement.service.impl.BookingServiceImpl.bookTickets(*))")
    private void bookTicketForEvent() {
    }

    @SuppressWarnings("unchecked")
    @After("bookTicketForEvent()")
    public void bookedTicketForEvent(JoinPoint point) {
        final String name = "bookTicketForEvent";
        Set<Object> objects = Arrays.stream(point.getArgs()).collect(Collectors.toCollection(HashSet::new));
        Set<Set<Ticket>> ticketsSet = objects.stream().map(object -> (Set<Ticket>) object).collect(Collectors.toSet());

        Event event = ticketsSet.iterator().next().iterator().next().getEvent();
        int counter = counterDAO.getValue(name);
        counter++;
        counterDAO.update(event, name, counter);
        LOGGER.info("worked counter aspect : tickets were booked for event");
    }
}
