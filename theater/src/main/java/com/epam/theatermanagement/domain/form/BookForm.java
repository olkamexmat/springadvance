package com.epam.theatermanagement.domain.form;

import java.io.Serializable;

public class BookForm implements Serializable {

	private static final long serialVersionUID = -4647072683697712591L;
	private String date;
	private String name;
	private Integer seat;

	public BookForm() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSeat() {
		return seat;
	}

	public void setSeat(Integer seat) {
		this.seat = seat;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}


	@Override
	public String toString() {
		return "BookForm [date=" + date + ", name=" + name + ", seat=" + seat + "]";
	}

}
