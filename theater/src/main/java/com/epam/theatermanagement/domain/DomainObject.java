package com.epam.theatermanagement.domain;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author Volha_Shautsova
 */
@Component
public class DomainObject implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
