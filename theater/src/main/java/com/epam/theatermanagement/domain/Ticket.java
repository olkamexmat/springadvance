package com.epam.theatermanagement.domain;

import javax.annotation.Nonnull;

import java.time.LocalDate;
import java.util.Objects;

/**
 * @author Volha_Shautsova
 */
public class Ticket extends DomainObject implements Comparable<Ticket> {

	private static final long serialVersionUID = 4298333418480765181L;

	private User user;

	private Event event;

	private LocalDate dateTime;

	private long seat;

	public Ticket() {
	}

	public Ticket(User user, Event event, LocalDate dateTime, long seat) {
		this.user = user;
		this.event = event;
		this.dateTime = dateTime;
		this.seat = seat;
	}

	public User getUser() {
		return user;
	}

	public Event getEvent() {
		return event;
	}

	public LocalDate getDateTime() {
		return dateTime;
	}

	public long getSeat() {
		return seat;
	}

	public void setDateTime(LocalDate dateTime) {
		this.dateTime = dateTime;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public void setSeat(long seat) {
		this.seat = seat;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dateTime, event, seat);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Ticket other = (Ticket) obj;
		if (dateTime == null) {
			if (other.dateTime != null) {
				return false;
			}
		} else if (!dateTime.equals(other.dateTime)) {
			return false;
		}
		if (event == null) {
			if (other.event != null) {
				return false;
			}
		} else if (!event.equals(other.event)) {
			return false;
		}
		return seat == other.seat;
	}

	@Override
	public int compareTo(@Nonnull Ticket ticket) {
		int result = dateTime.compareTo(ticket.getDateTime());

		if (result == 0) {
			result = event.getName().compareTo(ticket.getEvent().getName());
		}
		if (result == 0) {
			result = Long.compare(seat, ticket.getSeat());
		}
		return result;
	}

	@Override
	public String toString() {
		return "Ticket: \n" + "dateTime : " + dateTime + "\n user : " + user.getFirstName() + " " + user.getLastName()
				+ "\n event : " + event.getName() + "\n seat : " + seat + '\n';
	}
}
