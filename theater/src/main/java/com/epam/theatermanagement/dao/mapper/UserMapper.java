package com.epam.theatermanagement.dao.mapper;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;

import com.epam.theatermanagement.domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeSet;

public final class UserMapper implements RowMapper<User> {
	private static final String USER_ID = "USER_ID";
	private static final String FIRST_NAME = "FIRST_NAME";
	private static final String LAST_NAME = "LAST_NAME";
	private static final String EMAIL = "EMAIL";
	private static final String BIRTHDAY = "BIRTHDAY";
	private static final String PASSWORD = "PASSWORD";
    private static final String ROLE = "ROLE";
	
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException, DataAccessException {
		User user = new User();
		user.setId(rs.getLong(USER_ID));
		user.setFirstName(rs.getString(FIRST_NAME));
		user.setLastName(rs.getString(LAST_NAME));
		user.setEmail(rs.getString(EMAIL));
		user.setBirthday(rs.getDate(BIRTHDAY).toLocalDate());
		user.setPassword(rs.getString(PASSWORD));
        user.setRole(rs.getString(ROLE));
		user.setTickets(new TreeSet<>());
		return user;
	}

}
