package com.epam.theatermanagement.dao;

import com.epam.theatermanagement.dao.mapper.TicketMapper;
import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.Ticket;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Volha_Shautsova
 */
@Repository
public class TicketDAO implements DomainObjectDAO<Ticket> {
	private static final String SQL_INSERT_NEW_TICKET = "INSERT INTO Ticket (TICKET_DATE, SEAT, USER_ID, EVENT_ID)"
			+ " VALUES (?,?,?,?)";
	private static final String SQL_REMOVE_TICKET = "DELETE FROM Ticket WHERE TICKET_ID = ?";
	private static final String SQL_TAKE_ALL_TICKET = "SELECT TICKET_ID, TICKET_DATE, SEAT, Ticket.USER_ID, Ticket.EVENT_ID,"
			+ "FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, EVENT_NAME, BASE_PRICE, RATING "
			+ "FROM Ticket INNER JOIN Users ON Ticket.USER_ID = Users.USER_ID"
			+ " INNER JOIN Event ON Ticket.EVENT_ID = Event.EVENT_ID";
	private static final String SQL_TAKE_TICKET_BY_ID = "SELECT TICKET_ID, TICKET_DATE, SEAT, Ticket.USER_ID, Ticket.EVENT_ID,"
			+ "FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, EVENT_NAME, BASE_PRICE, RATING "
			+ "FROM Ticket INNER JOIN Users ON Ticket.USER_ID = Users.USER_ID "
			+ "INNER JOIN Event ON Ticket.EVENT_ID = Event.EVENT_ID WHERE Ticket.TICKET_ID = ?";
	private static final String SQL_GET_PURCHASED_TICKET = "SELECT TICKET_ID, TICKET_DATE, SEAT, Ticket.USER_ID, Ticket.EVENT_ID, "
			+ "FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, EVENT_NAME, BASE_PRICE, RATING "
			+ "FROM Ticket INNER JOIN Users ON Ticket.USER_ID = Users.USER_ID "
			+ "INNER JOIN Event ON Ticket.EVENT_ID = Event.EVENT_ID " + "WHERE (EVENT_ID = ?) AND (TICKET_DATE = ?)";

	@Resource(name = "getJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/**
	 * Getting purchased tickets for certain event and date from storage
	 *
	 * @param event
	 *            the event for which get tickets
	 * @param dateTime
	 *            the date for which find tickets
	 * @return the <code>Set</code> of founded ticket
	 */
	public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDate dateTime) {
		Set<Ticket> tickets = new HashSet<>();
		tickets.addAll(jdbcTemplate.query(SQL_GET_PURCHASED_TICKET,
				new Object[] { event.getId(), java.sql.Date.valueOf(dateTime) }, new TicketMapper()));
		return tickets;
	}

	@Override
	public Ticket save(@Nonnull Ticket ticket) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(connection -> {
			PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_NEW_TICKET,
					new String[] { "TICKET_ID" });
			
			preparedStatement.setDate(1, java.sql.Date.valueOf(ticket.getDateTime()));
			preparedStatement.setInt(2,  (int) ticket.getSeat());
			preparedStatement.setFloat(3, ticket.getUser().getId());
			preparedStatement.setFloat(4, ticket.getEvent().getId());

			return preparedStatement;
		}, keyHolder);
		ticket.setId(keyHolder.getKey().longValue());
		return ticket;
	}

	@Override
	public void remove(@Nonnull Ticket ticket) {
		jdbcTemplate.update(SQL_REMOVE_TICKET, ticket.getId());
	}

	@Override
	public Ticket getById(@Nonnull Long id) {
		return jdbcTemplate.queryForObject(SQL_TAKE_TICKET_BY_ID, new Object[] { id }, new TicketMapper());
	}

	@Nonnull
	@Override
	public Collection<Ticket> getAll() {
		return jdbcTemplate.query(SQL_TAKE_ALL_TICKET, new TicketMapper());
	}

}
