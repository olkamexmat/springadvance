package com.epam.theatermanagement.dao.mapper;


import org.springframework.jdbc.core.RowMapper;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.EventRating;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class EventMapper implements RowMapper<Event> {
    private static final String EVENT_ID = "EVENT_ID";
    private static final String EVENT_NAME = "EVENT_NAME";
    private static final String BASE_PRICE = "BASE_PRICE";
    private static final String RATING = "RATING";

    @Override
    public Event mapRow(ResultSet resultSet, int i) throws SQLException {
        Event event = new Event();
        event.setId(resultSet.getLong(EVENT_ID));
        event.setName(resultSet.getString(EVENT_NAME));
        event.setBasePrice(resultSet.getDouble(BASE_PRICE));
        event.setRating(EventRating.valueOf(resultSet.getString(RATING).toUpperCase()));
        return event;
    }
}
