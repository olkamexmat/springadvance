package com.epam.theatermanagement.dao.mapper;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.jdbc.core.RowMapper;

import com.epam.theatermanagement.domain.Auditorium;

public final class AuditoriumMapper implements RowMapper<Auditorium> {
	private static final String ID = "AUDITORIUM_ID";
	private static final String NAME = "AUDITORIUM_NAME";
	private static final String VIP_SEATS = "VIP_SEATS";
	private static final String SEATS_AMOUNT = "AMOUNT_OF_SEATS";

	@Override
	public Auditorium mapRow(ResultSet rs, int rowNum) throws SQLException {
		Auditorium auditorium = new Auditorium();
		auditorium.setId(rs.getLong(ID));
		auditorium.setName(rs.getString(NAME));
		auditorium.setNumberOfSeats(rs.getInt(SEATS_AMOUNT));
		auditorium.setVipSeats(getVipSeats(rs.getArray(VIP_SEATS)));
		
		return auditorium;
	}
	
	private Set<Long> getVipSeats(Array vipSeatsArray) throws NumberFormatException, SQLException {
		List<Long> seatsLong = new ArrayList<>();
		for (Object obj : (Object[]) vipSeatsArray.getArray()) {
			try {
				seatsLong.add(Long.parseLong(obj.toString()));
			} catch (ClassCastException e) {
				e.printStackTrace();
			}
		}
		return new HashSet<Long>(seatsLong);
	}

}
