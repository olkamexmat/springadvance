package com.epam.theatermanagement.dao;

import com.epam.theatermanagement.dao.mapper.EventMapper;
import com.epam.theatermanagement.domain.Event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Resource;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.Collection;

/**
 * @author Volha_Shautsova
 */
@Repository
public class EventDAO implements DomainObjectDAO<Event> {
	private static final String SQL_INSERT_NEW_EVENT = "INSERT INTO Event ( EVENT_NAME, BASE_PRICE, RATING) "
			+ "VALUES (?,?,?)";
	private static final String SQL_INSERT_AIR_DATE = "INSERT INTO Air_Date ( AIR_DATE) VALUES (?)";
	private static final String SQL_LINK_EVENT_AND_AIR_DATE = "INSERT INTO Event_Date (EVENT_ID, AIR_DATE_ID) VALUES (?,?)";
	private static final String SQL_SELECT_EVENT_BY_NAME = "SELECT EVENT_ID, EVENT_NAME, BASE_PRICE, RATING FROM Event WHERE EVENT_NAME = ?";
	private static final String SQL_FIND_ALL_EVENT = "SELECT EVENT_ID, EVENT_NAME, BASE_PRICE, RATING FROM Event";
	private static final String SQL_REMOVE_EVENT = "DELETE FROM Event WHERE EVENT_ID = ?";
	private static final String SQL_REMOVE_EVENT_DATE = "DELETE FROM Event_Date WHERE EVENT_ID = ?";
	private static final String SQL_FIND_EVENT_BY_ID = "SELECT EVENT_ID, EVENT_NAME, BASE_PRICE, RATING FROM Event WHERE EVENT_ID = ?";
	private static final String SQL_UPDATE_EVENT = "UPDATE Event SET EVENT_NAME = ?, BASE_PRICE = ?, RATING = ? WHERE EVENT_ID = ?";

	@Resource(name = "getJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private AirDateDAO airDateDAO;

	/**
	 * Getting event from storage by id
	 *
	 * @param name
	 *            event name
	 * @return event that have the same name with <code>name</code>
	 * @see com.epam.theatermanagement.domain.Event
	 */
	public Event getByName(String name) {
		return jdbcTemplate.queryForObject(SQL_SELECT_EVENT_BY_NAME, new Object[] { name }, new EventMapper());
	}

	@Override
	public Event save(@Nonnull Event event) {
		KeyHolder eventKeyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(connection -> {
			PreparedStatement statement = connection.prepareStatement(SQL_INSERT_NEW_EVENT,
					new String[] { "EVENT_ID" });
			statement.setString(1, event.getName());
			statement.setDouble(2, event.getBasePrice());
			statement.setString(3, event.getRating().toString());
			return statement;
		}, eventKeyHolder);
		event.setId(eventKeyHolder.getKey().longValue());

		if (!event.getAirDates().isEmpty()) {
			for (LocalDate date : event.getAirDates()) {
				KeyHolder keyHolder = new GeneratedKeyHolder();
				jdbcTemplate.update(connection -> {
					PreparedStatement statement = connection.prepareStatement(SQL_INSERT_AIR_DATE,
							new String[] { "AIR_DATE_ID" });
					statement.setDate(1, java.sql.Date.valueOf(date));
					return statement;
				}, keyHolder);
				jdbcTemplate.update(SQL_LINK_EVENT_AND_AIR_DATE, event.getId(), keyHolder.getKey().longValue());
			}
		}
		return event;
	}

	@Override
	public void remove(@Nonnull Event event) {
		jdbcTemplate.update(SQL_REMOVE_EVENT_DATE, event.getId());
		jdbcTemplate.update(SQL_REMOVE_EVENT, event.getId());
	}

	@Override
	public Event getById(@Nonnull Long id) {
		Event event = jdbcTemplate.queryForObject(SQL_FIND_EVENT_BY_ID, new Object[] { id }, new EventMapper());
		event.setAirDates(airDateDAO.getAirDates(event.getId()));
		return event;
	}

	@Nonnull
	@Override
	public Collection<Event> getAll() {
		Collection<Event> events = jdbcTemplate.query(SQL_FIND_ALL_EVENT, new EventMapper());
		for (Event event : events) {
			event.setAirDates(airDateDAO.getAirDates(event.getId()));
		}
		return events;
	}

	public void update(@Nonnull Long id, @Nonnull Event event) {
		jdbcTemplate.update(SQL_UPDATE_EVENT,
                event.getName(), event.getBasePrice(), event.getRating().toString(), id);
	}
}
