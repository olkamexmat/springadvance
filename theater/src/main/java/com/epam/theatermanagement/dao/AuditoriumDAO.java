package com.epam.theatermanagement.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.epam.theatermanagement.dao.mapper.AuditoriumMapper;
import com.epam.theatermanagement.domain.Auditorium;
import com.epam.theatermanagement.domain.Event;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Volha_Shautsova
 */
@Repository
public class AuditoriumDAO {
	private static final String SQL_TAKE_ALL_AUDITORIUM = "SELECT AUDITORIUM_ID, AUDITORIUM_NAME, VIP_SEATS, AMOUNT_OF_SEATS FROM AUDITORIUM";
	private static final String SQL_TAKE_AUDITORIUM_BY_NAME = "SELECT AUDITORIUM_ID, AUDITORIUM_NAME, VIP_SEATS, AMOUNT_OF_SEATS FROM AUDITORIUM WHERE AUDITORIUM_NAME = ?";
	private static final String SQL_TAKE_AUDITORIUM_BY_EVENT = "SELECT AUDITORIUM.AUDITORIUM_ID, AUDITORIUM_NAME, VIP_SEATS, AMOUNT_OF_SEATS FROM AUDITORIUM JOIN AUDITORIUM_EVENT ON AUDITORIUM.AUDITORIUM_ID = AUDITORIUM_EVENT.AUDITORIUM_ID WHERE AUDITORIUM_EVENT.EVENT_ID = ?";
	
	@Resource(name = "getJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
    /**
     * @return all auditoriums in storage <code>Set<Auditorium></code>
     */
    public Set<Auditorium> getAll() {
    	Set<Auditorium> auditoriums = new LinkedHashSet<>();
        auditoriums.addAll(jdbcTemplate.query(SQL_TAKE_ALL_AUDITORIUM, new AuditoriumMapper()));
        return auditoriums;
    }

    /**
     * @param name name of auditorium
     * @return auditorium
     * if no any matches @return <code>null</code>
     */
    public Auditorium getByName(String name) {
        return jdbcTemplate.queryForObject(SQL_TAKE_AUDITORIUM_BY_NAME, new Object[] {name}, new AuditoriumMapper());
    }

    public Event assignAuditorium(@Nonnull Event event, String auditoriumName) {
    	for (LocalDate date : event.getAirDates()) {
			event.assignAuditorium(date, getByName(auditoriumName));
		}
    	return event;
    }
    
    public Event assignAuditorium(@Nonnull Event event) {
    	for (LocalDate date : event.getAirDates()) {
    		for (Auditorium aud : getByEvent(event)) {
    			event.assignAuditorium(date, aud);
			}
		}
    	return event;
    }
    
    public List<Auditorium> getByEvent(@Nonnull Event event) {
    	return jdbcTemplate.query(SQL_TAKE_AUDITORIUM_BY_EVENT,  new Object[] {event.getId()}, new AuditoriumMapper());
    }

}
