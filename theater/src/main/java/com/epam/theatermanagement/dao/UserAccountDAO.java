package com.epam.theatermanagement.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.epam.theatermanagement.dao.mapper.AccountMapper;
import com.epam.theatermanagement.domain.UserAccount;
import com.epam.theatermanagement.exceptions.DAOException;

@Repository
public class UserAccountDAO implements DomainObjectDAO<UserAccount> {
	private static final Logger LOGGER = LogManager.getLogger(UserAccountDAO.class);

	private static final String SQL_DELETE_USER_ACCOUNT = "DELETE FROM UserAccount WHERE ACCOUNT_ID = ?";
	private static final String SQL_INSERT_NEW_ACCOUNT = "INSERT INTO UserAccount (USER_ID, CASH) VALUES (?,?)";
	private static final String SQL_GET_ACCOUNT_BY_ID = "SELECT ACCOUNT_ID, Users.USER_ID, Users.FIRST_NAME, Users.LAST_NAME, "
			+ "Users.EMAIL, Users.BIRTHDAY, Users.PASSWORD, Users.ROLE, CASH"
			+ " FROM UserAccount INNER JOIN Users ON Users.USER_ID = UserAccount.USER_ID WHERE ACOOUNT_ID = ?";
	private static final String SQL_GET_ALL_ACCOUNTS = "SELECT ACCOUNT_ID, Users.USER_ID, Users.FIRST_NAME, Users.LAST_NAME, "
			+ "Users.EMAIL, Users.BIRTHDAY, Users.PASSWORD, Users.ROLE, CASH"
			+ " FROM UserAccount INNER JOIN Users ON Users.USER_ID = UserAccount.USER_ID";
	private static final String SQL_UPDATE_CASH = "UPDATE UserAccount SET CASH = ? WHERE ACCOUNT_ID = ?";
	private static final String SQL_GET_ACCOUNT_BY_USER = "SELECT ACCOUNT_ID, Users.USER_ID, "
			+ "Users.FIRST_NAME, Users.LAST_NAME, " + "Users.EMAIL, Users.BIRTHDAY, Users.PASSWORD, Users.ROLE, CASH"
			+ " FROM UserAccount INNER JOIN Users ON Users.USER_ID = UserAccount.USER_ID WHERE UserAccount.USER_ID = ?";

	@Resource(name = "getJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/**
	 * Adding money to user cash
	 * 
	 * @param money
	 * @param account
	 */
	public void update(double money, UserAccount account) {
		jdbcTemplate.update(SQL_UPDATE_CASH, account.getCash() + money, account.getId());
	}

	/**
	 * Getting account per certain user
	 * 
	 * @param userId
	 * @return
	 */
	public UserAccount getByUserId(Long userId) {
		return jdbcTemplate.queryForObject(SQL_GET_ACCOUNT_BY_USER, new Object[] { userId }, new AccountMapper());
	}

	@Override
	public UserAccount save(UserAccount account) throws DAOException {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(connection -> {
			try {
				PreparedStatement ps = connection.prepareStatement(SQL_INSERT_NEW_ACCOUNT,
						new String[] { "ACCOUNT_ID" });
				ps.setLong(1, account.getUser().getId());
				ps.setDouble(2, account.getCash());
				return ps;
			} catch (SQLException exc) {
				LOGGER.error("Error userAccount saving, exception is " + exc.getMessage());
				throw new DAOException("Error account saving", exc);
			}
		}, keyHolder);

		account.setId(keyHolder.getKey().longValue());
		return account;
	}

	@Override
	public void remove(UserAccount account) {
		try {
			jdbcTemplate.update(SQL_DELETE_USER_ACCOUNT, account.getId());
		} catch (DataAccessException exception) {
			throw new EmptyResultDataAccessException("Incorrect result size", 0);
		}
	}

	@Override
	public UserAccount getById(Long id) {
		return jdbcTemplate.queryForObject(SQL_GET_ACCOUNT_BY_ID, new Object[] { id }, new AccountMapper());
	}

	@Override
	public Collection<UserAccount> getAll() {
		return jdbcTemplate.query(SQL_GET_ALL_ACCOUNTS, new AccountMapper());
	}

}
