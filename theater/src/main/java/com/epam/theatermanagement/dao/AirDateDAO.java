package com.epam.theatermanagement.dao;

import java.time.LocalDate;
import java.util.Collection;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.epam.theatermanagement.dao.mapper.DateMapper;

/**
 * 
 * @author Volha_Shautsova
 *
 */
@Repository
public class AirDateDAO {
	private static final String SQL_SELECT_DATES_FOR_EVENT = "SELECT AIR_DATE "
			+ "FROM Air_Date JOIN Event_Date ON Air_Date.AIR_DATE_ID = Event_Date.AIR_DATE_ID " + "WHERE EVENT_ID = ?";
	private static final String SQL_SELECT_ALL_DATES = "SELECT DISTINCT AIR_DATE FROM Air_Date";
	
	@Resource(name = "getJdbcTemplate")
	private JdbcTemplate jdbcTemplate;
	
	public Collection<LocalDate> getAllDates() {
		return jdbcTemplate.query(SQL_SELECT_ALL_DATES, new DateMapper());
	}

	public NavigableSet<LocalDate> getAirDates(Long id) {
		Collection<LocalDate> dates = jdbcTemplate.query(SQL_SELECT_DATES_FOR_EVENT, new Object[] { id }, new DateMapper());
		NavigableSet<LocalDate> airDates = new TreeSet<>();
		airDates.addAll(dates.stream().collect(Collectors.toSet()));
		return airDates;
	}
}
