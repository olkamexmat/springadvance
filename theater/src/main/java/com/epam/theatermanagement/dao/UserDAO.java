package com.epam.theatermanagement.dao;

import com.epam.theatermanagement.dao.mapper.UserMapper;
import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.exceptions.DAOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Resource;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * @author Volha_Shautsova
 */
@Repository
public class UserDAO implements DomainObjectDAO<User> {
	private static final Logger LOGGER = LogManager.getLogger(UserDAO.class);
	private static final String SQL_GET_USER_BY_EMAIL = "SELECT USER_ID, FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, PASSWORD, ROLE FROM Users WHERE EMAIL = ?";
	private static final String SQL_INSERT_NEW_USER = "INSERT INTO Users ( FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, BIRTHDAY)"
			+ " VALUES (?,?,?,?,?)";
	private static final String SQL_GET_USER_BY_ID = "SELECT USER_ID, FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, PASSWORD, ROLE FROM Users WHERE USER_ID = ?";
	private static final String SQL_DELETE_USER = "DELETE FROM Users WHERE USER_ID = ?";
	private static final String SQL_FIND_ALL_USERS = "SELECT USER_ID, FIRST_NAME, LAST_NAME, EMAIL, BIRTHDAY, PASSWORD, ROLE FROM Users";

	@Resource(name = "getJdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	/**
	 * Getting user from storage by certain email
	 * 
	 * @see com.epam.theatermanagement.domain.User
	 *
	 * @param email
	 *            the searching criteria for user
	 * @return User with email <code>email</code>
	 * @throws DAOException
	 */
	public User getUserByEmail(String email) throws DAOException {
		List<User> users = jdbcTemplate.query(SQL_GET_USER_BY_EMAIL, new Object[] { email }, new UserMapper());

		int end = users.size();
		switch (end) {
		case 0:
			throw new DAOException("User not found");
		case 1:
			return users.get(0);
		default:
			throw new DAOException("More then one user found");
		}
	}

	@Override
	public User save(@Nonnull User user) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(connection -> {
			try {
				PreparedStatement ps = connection.prepareStatement(SQL_INSERT_NEW_USER, new String[] { "USER_ID" });
				ps.setString(1, user.getFirstName());
				ps.setString(2, user.getLastName());
				ps.setString(3, user.getEmail());
				ps.setString(4, user.getPassword());
				ps.setDate(5, java.sql.Date.valueOf(user.getBirthday()));
				return ps;
			} catch (SQLException exc) {
				LOGGER.error("Error user saving, exception is " + exc.getMessage());
				throw new DAOException("Error user saving", exc);
			}
		}, keyHolder);

		user.setId(keyHolder.getKey().longValue());
		return user;
	}

	@Override
	public void remove(@Nonnull User user) {
		try {
			jdbcTemplate.update(SQL_DELETE_USER, user.getId());
		} catch (DataAccessException exception) {
			throw new EmptyResultDataAccessException("Incorrect result size", 0);
		}
	}

	@Override
	public User getById(@Nonnull Long id) {
		return jdbcTemplate.queryForObject(SQL_GET_USER_BY_ID, new Object[] { id }, new UserMapper());
	}

	@Nonnull
	@Override
	public Collection<User> getAll() {
		return jdbcTemplate.query(SQL_FIND_ALL_USERS, new UserMapper());
	}
}
