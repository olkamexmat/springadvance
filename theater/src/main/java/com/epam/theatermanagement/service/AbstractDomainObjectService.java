package com.epam.theatermanagement.service;

import com.epam.theatermanagement.domain.DomainObject;
import com.epam.theatermanagement.exceptions.ServiceException;

import javax.annotation.Nonnull;
import java.util.Collection;

/**
 * @param <T>
 *            DomainObject subclass
 * @author Volha_Shautsova
 */
public interface AbstractDomainObjectService<T extends DomainObject> {

	/**
	 * Saving new object to storage or updating existing one
	 *
	 * @param object
	 *            Object to save
	 * @return saved object with assigned id
	 * @throws ServiceException
	 */
	T save(@Nonnull T object) throws ServiceException;

	/**
	 * Removing object from storage
	 *
	 * @param object
	 *            Object to remove
	 */
	void remove(@Nonnull T object);

	/**
	 * Getting object by id from storage
	 *
	 * @param id
	 *            id of the object
	 * @return Found object or <code>null</code>
	 */
	T getById(@Nonnull Long id);

	/**
	 * Getting all objects from storage
	 *
	 * @return collection of objects
	 */
	@Nonnull
	Collection<T> getAll();
}
