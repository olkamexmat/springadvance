package com.epam.theatermanagement.service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.epam.theatermanagement.domain.Event;

/**
 * @author Volha_Shautsova
 */
public interface EventService extends AbstractDomainObjectService<Event> {

	/**
	 * Finding event by name
	 *
	 * @param name
	 *            Name of the event
	 * @return found event or <code>null</code>
	 */
	@Nullable
	Event getByName(@Nonnull String name);

}
