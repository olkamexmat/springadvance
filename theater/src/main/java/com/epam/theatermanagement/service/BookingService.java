package com.epam.theatermanagement.service;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.Ticket;
import com.epam.theatermanagement.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.util.Set;

/**
 * @author Volha_Shautsova
 */
public interface BookingService {

	/**
	 * Getting price when buying all supplied seats for particular event
	 * 
	 * @param event
	 *            Event to get base ticket price, vip seats and other
	 *            information
	 * @param dateTime
	 *            Date and time of event air
	 * @param user
	 *            User that buys ticket could be needed to calculate discount.
	 *            Can be <code>null</code>
	 * @param seats
	 *            Set of seat numbers that user wants to buy
	 * @return total price
	 */
	double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDate dateTime, @Nullable User user,
			@Nonnull Long seats);

	/**
	 * Books tickets in internal system. If user is not <code>null</code> in a
	 * ticket then booked tickets are saved with it
	 * 
	 * @param tickets
	 *            Set of tickets
	 * @throws Exception 
	 */
	void bookTickets(@Nonnull Set<Ticket> tickets) throws Exception;

	/**
	 * Getting all purchased tickets for event on specific air date and time
	 * 
	 * @param event
	 *            Event to get tickets for
	 * @param dateTime
	 *            Date and time of airing of event
	 * @return set of all purchased tickets
	 */
	@Nonnull
	Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDate dateTime);

	/**
	 * Getting available money for user
	 *  
	 * @param user
	 * @return user's cash
	 */
	double getUserCash(@Nonnull User user);
}
