package com.epam.theatermanagement.service.impl;

import com.epam.theatermanagement.discount.DiscountStrategy;
import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.service.DiscountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.*;

/**
 * @author Volha_Shautsova
 */
@Service
@Transactional
public class DiscountServiceImpl implements DiscountService {
    private static final double FOR_PERCENT = 0.01;
    /**
     * final strategy that give final discount
     */
    private DiscountStrategy totalStrategy;
    /**
     * contains DiscountStrategy as key execute discount and as value
     */
    private Map<DiscountStrategy, Integer> discounts = new HashMap<>();
    /**
     * user as key, value is map with strategy as key
     * and counter(how many times strategy gave final discount) as value
     */
    private static Map<User, Map<DiscountStrategy, Integer>> userMap = new HashMap<>();

    private Set<Integer> discountsByStrategies = new HashSet<>();
    /**
     * the final discount
     */
    private double totalDiscount;

    @Autowired
    private List<DiscountStrategy> strategies;

    public double getDiscount(User user, @Nonnull Event event, @Nonnull LocalDate airDateTime, long numberOfTickets) {
        for (DiscountStrategy strategy : strategies) {
            int discount = strategy.executeStrategy(user, event, airDateTime, numberOfTickets);
            discountsByStrategies.add(discount);
            discounts.put(strategy, discount);
        }
        totalDiscount = (Collections.max(discountsByStrategies) * FOR_PERCENT);
        int counter;
        if (userMap.isEmpty()) {
            counter = 0;
        } else {
            counter = userMap.get(user).get(getTypeStrategy());
        }

        discounts.put(getTypeStrategy(), ++counter);

        userMap.put(user, discounts);
        return totalDiscount == 0 ? 1 : totalDiscount;
    }

    public List<DiscountStrategy> getStrategies() {
        return strategies;
    }

    public DiscountStrategy getTypeStrategy() {
        discounts.entrySet().stream().filter(entry -> entry.getValue() == totalDiscount).forEach(entry -> {
            totalStrategy = entry.getKey();
        });
        return totalStrategy;
    }

    public static Map<User, Map<DiscountStrategy, Integer>> getUserMap() {
        return userMap;
    }

    public void setStrategy(List<DiscountStrategy> strategy) {
        strategies = strategy;
    }
}
