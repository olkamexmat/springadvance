package com.epam.theatermanagement.service;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.exceptions.ServiceException;
import com.epam.theatermanagement.parser.EventParser;
import com.epam.theatermanagement.parser.UsersParser;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class FileService {
	@Autowired
	private UserService userService;

	@Autowired
	private EventService eventService;

	/**
	 * Parsing JSON files to batch users or events to system
	 * 
	 * @param fileMap
	 * @return <code>List</code> of users or events
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws ServiceException
	 */
	public List<String> parse(Map<String, MultipartFile> fileMap)
			throws JsonParseException, JsonMappingException, IOException, ServiceException {
		List<String> data = new ArrayList<>();

		for (String fileName : fileMap.keySet()) {
			if (("users").equals(fileName)) {
				String userData = read(fileMap.get(fileName).getInputStream());
				List<User> users = UsersParser.getUsers(userData);
				for (User user : users) {
					userService.save(user);
				}
				data.add(userData);
			}
			if (("events").equals(fileName)) {
				String eventData = read(fileMap.get(fileName).getInputStream());
				List<Event> events = EventParser.getEvents(eventData);
				for (Event event : events) {
					eventService.save(event);
				}
				data.add(eventData);
			}
		}
		return data;
	}

	private String read(InputStream inputStream) throws IOException {
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
			return bufferedReader.lines().collect(Collectors.joining("\n"));
		}
	}
}
