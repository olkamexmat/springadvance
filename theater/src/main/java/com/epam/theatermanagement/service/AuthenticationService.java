package com.epam.theatermanagement.service;

import com.epam.theatermanagement.dao.UserDAO;
import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.security.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService implements UserDetailsService {
	@Autowired
	private UserDAO userDAO;

	/**
	 * Getting Secure object (User) during login procedure
	 */
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userDAO.getUserByEmail(userName);

		if (user == null) {
			throw new UsernameNotFoundException("User with Login " + userName + " not found");
		}
		return new SecurityUser(user);
	}
}
