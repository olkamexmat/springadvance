package com.epam.theatermanagement.service.impl;

import com.epam.theatermanagement.dao.AuditoriumDAO;
import com.epam.theatermanagement.domain.Auditorium;
import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.service.AuditoriumService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;

import java.util.Set;

/**
 * @author Volha_Shautsova
 */
@Service
@Transactional
public class AuditoriumServiceImpl implements AuditoriumService {
    @Autowired
    private AuditoriumDAO auditoriumDAO;

    /**
     * Getting all auditoriums from the system (auditorium.properties)
     *
     * @return set of all auditoriums
     */
    @Nonnull
    @Override
    public Set<Auditorium> getAll() {
        return auditoriumDAO.getAll();
    }

    /**
     * Finding auditorium by name
     *
     * @param name Name of the auditorium
     * @return found auditorium or <code>null</code>
     */
    @Override
    public Auditorium getByName(@Nonnull String name) {
        return auditoriumDAO.getByName(name);
    }
    
    public void assignAuditorium(@Nonnull Event event, String auditoriumName) {
    	auditoriumDAO.assignAuditorium(event, auditoriumName);
    }
}
