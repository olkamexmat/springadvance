package com.epam.theatermanagement.service;

import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.exceptions.ServiceException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Volha_Shautsova
 */
public interface UserService extends AbstractDomainObjectService<User> {

	/**
	 * Finding user by email
	 *
	 * @param email
	 *            Email of the user
	 * @return found user or <code>null</code>
	 * @throws ServiceException
	 */
	@Nullable
	User getUserByEmail(@Nonnull String email) throws ServiceException;

}
