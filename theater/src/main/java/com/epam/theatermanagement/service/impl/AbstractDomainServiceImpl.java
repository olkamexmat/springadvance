package com.epam.theatermanagement.service.impl;

import org.springframework.stereotype.Service;

import com.epam.theatermanagement.domain.DomainObject;

/**
 * @param <T>
 * @author Volha_Shautsova
 */
@Service
public class AbstractDomainServiceImpl<T extends DomainObject> {

    /**
     * set unique id for object
     *
     * @param object for establish its id
     */
    void setId(T object) {
        object.setId(object.getId());
    }
}
