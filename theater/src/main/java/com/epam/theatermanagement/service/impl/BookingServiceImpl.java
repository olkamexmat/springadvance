package com.epam.theatermanagement.service.impl;

import com.epam.theatermanagement.dao.TicketDAO;
import com.epam.theatermanagement.domain.*;
import com.epam.theatermanagement.service.BookingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;

import java.time.LocalDate;
import java.util.Set;

/**
 * @author Volha_Shautsova
 */
@Service
@Transactional
public class BookingServiceImpl extends AbstractDomainServiceImpl<Ticket> implements BookingService {
	private static final double RATING_MULTIPLIER = 1.2;
	private static final double VIP_SEATS_MULTIPLIER = 2;
	@Autowired
	private TicketDAO ticketDAO;

	@Autowired
	private UserAccountServiceImpl userAccountServiceImpl;

	@Override
	public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDate dateTime, User user, @Nonnull Long seat) {
		double discount = 1;

		double price = event.getBasePrice();

		if (event.getRating() == EventRating.HIGH) {
			price *= RATING_MULTIPLIER;
		}

		Auditorium auditorium = event.getAuditoriums().get(dateTime);
		Set<Long> vipSeatsInAuditorium = auditorium.getVipSeats();

		if (vipSeatsInAuditorium.contains(seat)) {
			price += VIP_SEATS_MULTIPLIER * price;
		} else {
			price += price;
		}

		return price * discount;
	}

	@Override
	@Transactional
	public void bookTickets(@Nonnull Set<Ticket> tickets) throws Exception {
		for (Ticket ticket : tickets) {
			double price = 0;
			price += getTicketsPrice(ticket.getEvent(), ticket.getDateTime(), ticket.getUser(), ticket.getSeat());
			if (price <= getUserCash(ticket.getUser())) {
				ticketDAO.save(ticket);
				userAccountServiceImpl.update(-price, userAccountServiceImpl.getByUserId(ticket.getUser().getId()));
			} else {
				throw new Exception("Money is not enough! Rollback transaction.");
			}
		}
	}

	@Transactional
	public void bookTicket(@Nonnull Ticket ticket) throws Exception {
		double price = getTicketsPrice(ticket.getEvent(), ticket.getDateTime(), ticket.getUser(), ticket.getSeat());
		if (price <= getUserCash(ticket.getUser())) {
			ticketDAO.save(ticket);
			userAccountServiceImpl.update(-price, userAccountServiceImpl.getByUserId(ticket.getUser().getId()));
		} else {
			throw new Exception("Money is not enough! Rollback transaction.");
		}
	}

	@Nonnull
	@Override
	public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDate dateTime) {
		return ticketDAO.getPurchasedTicketsForEvent(event, dateTime);
	}

	@Override
	public double getUserCash(User user) {
		return (userAccountServiceImpl.getByUserId(user.getId())).getCash();
	}
}
