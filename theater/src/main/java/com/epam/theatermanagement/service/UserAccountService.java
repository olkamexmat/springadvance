package com.epam.theatermanagement.service;

import com.epam.theatermanagement.domain.UserAccount;

public interface UserAccountService extends AbstractDomainObjectService<UserAccount> {

}
