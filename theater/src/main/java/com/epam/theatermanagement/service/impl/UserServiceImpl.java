package com.epam.theatermanagement.service.impl;

import com.epam.theatermanagement.dao.UserDAO;
import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.exceptions.DAOException;
import com.epam.theatermanagement.exceptions.ServiceException;
import com.epam.theatermanagement.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;

import java.util.Collection;

/**
 * @author Volha_Shautsova
 */
@Service
@Transactional
public class UserServiceImpl extends AbstractDomainServiceImpl<User> implements UserService {
	@Autowired
	private UserDAO userDAO;

	@Override
	public User save(@Nonnull User user) throws ServiceException {
		User newUser = userDAO.save(user);
		if (newUser != null) {
			return newUser;
		} else {
			throw new ServiceException("User saving error");
		}
	}

	@Override
	public void remove(@Nonnull User user) {
		userDAO.remove(user);
	}

	@Override
	public User getById(@Nonnull Long id) {
		return userDAO.getById(id);
	}

	@Nonnull
	@Override
	public Collection<User> getAll() {
		return userDAO.getAll();
	}

	/**
	 * Finding user by email
	 *
	 * @param email
	 *            Email of the user
	 * @return found user or <code>null</code>
	 * @throws ServiceException
	 */
	@Override
	public User getUserByEmail(@Nonnull String email) throws ServiceException {
		try {
			return userDAO.getUserByEmail(email);
		} catch (DAOException exc) {
			throw new ServiceException("Error finding user");
		}

	}

}
