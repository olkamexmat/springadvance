package com.epam.theatermanagement.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.theatermanagement.dao.UserAccountDAO;
import com.epam.theatermanagement.domain.UserAccount;
import com.epam.theatermanagement.exceptions.ServiceException;
import com.epam.theatermanagement.service.UserAccountService;

@Service
public class UserAccountServiceImpl implements UserAccountService {

	@Autowired
	private UserAccountDAO userAccountDAO;

	public void update(double money, UserAccount account) {
		userAccountDAO.update(money, account);
	}

	public UserAccount getByUserId(Long userId) {		
		return userAccountDAO.getByUserId(userId);
	}

	@Override
	public UserAccount save(UserAccount account) throws ServiceException {
		return userAccountDAO.save(account);
	}

	@Override
	public void remove(UserAccount account) {
		userAccountDAO.remove(account);
	}

	@Override
	public UserAccount getById(Long id) {
		return userAccountDAO.getById(id);
	}

	@Override
	public Collection<UserAccount> getAll() {
		return userAccountDAO.getAll();
	}

}
