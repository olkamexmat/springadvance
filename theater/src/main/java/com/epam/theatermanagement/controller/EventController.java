package com.epam.theatermanagement.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.Ticket;
import com.epam.theatermanagement.service.impl.BookingServiceImpl;
import com.epam.theatermanagement.service.impl.EventServiceImpl;

@Controller
public class EventController {
	@Autowired
	private EventServiceImpl eventServiceImpl;
	@Autowired
	private BookingServiceImpl bookingServiceImpl;

	@RequestMapping(value = "/events", method = RequestMethod.GET)
	public ModelAndView viewEvents(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModelAndView model = new ModelAndView("events");
		model.addObject("events", eventServiceImpl.getAll());	
		
		return model;
	}

	@RequestMapping(value = "/events/{id}/tickets", method = RequestMethod.GET)
	public ModelAndView viewTickets(@PathVariable("id") Long eventId) {
		ModelAndView model = new ModelAndView("ticketsForEvent");
		Event event = eventServiceImpl.getById(eventId);
		Map<LocalDate, Set<Ticket>> tickets = new HashMap<>();
		for (LocalDate date : event.getAirDates()) {
			tickets.put(date, bookingServiceImpl.getPurchasedTicketsForEvent(event, date));
		}		
		model.addObject("tickets", tickets);

		return model;
	}

	@RequestMapping(value = "/events/{id}/tickets", method = RequestMethod.GET, headers = "accept = application/pdf")
	public ModelAndView viewTicketsPDF(@PathVariable("id") Long eventId) {
		ModelAndView model = new ModelAndView("ticketsForEvent");
		Event event = eventServiceImpl.getById(eventId);
		Map<LocalDate, Set<Ticket>> tickets = new HashMap<>();
		for (LocalDate date : event.getAirDates()) {
			tickets.put(date, bookingServiceImpl.getPurchasedTicketsForEvent(event, date));
		}
		model.addObject("tickets", tickets);

		return model;
	}
}
