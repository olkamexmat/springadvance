package com.epam.theatermanagement.controller;

import com.epam.theatermanagement.domain.Event;
import com.epam.theatermanagement.domain.Ticket;
import com.epam.theatermanagement.domain.User;
import com.epam.theatermanagement.domain.form.BookForm;
import com.epam.theatermanagement.service.impl.AuditoriumServiceImpl;
import com.epam.theatermanagement.service.impl.BookingServiceImpl;
import com.epam.theatermanagement.service.impl.EventServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@Controller
public class BookingController {
	@Autowired
	private EventServiceImpl eventServiceImpl;
	@Autowired
	private AuditoriumServiceImpl auditoriumServiceImpl;
	@Autowired
	private BookingServiceImpl bookingServiceImpl;

	@RequestMapping(value = "/events/book/{id}", method = RequestMethod.GET)
	public ModelAndView bookTicket(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long eventId, @ModelAttribute("bookForm") BookForm bookForm)
			throws ServletException, IOException {
		ModelAndView model = new ModelAndView("eventForBook");
		Event event = eventServiceImpl.getById(eventId);
		model.addObject("bookForm", new BookForm());
		model.addObject("event", event);
		model.addObject("dates", event.getAuditoriums().keySet());		
		model.addObject("auditoriums", auditoriumServiceImpl.getAll());
		
		return model;
	}

	@RequestMapping(value = "/events/book/{id}", method = RequestMethod.POST)
	public ModelAndView bookWithDate(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("bookForm") BookForm bookForm, BindingResult result, @PathVariable("id") Long eventId)
			throws Exception {
		ModelAndView model = new ModelAndView("showTicket");
		Event event = eventServiceImpl.getById(eventId);
		LocalDate date = LocalDate.parse(bookForm.getDate());
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Ticket ticket = new Ticket(user, event, date, bookForm.getSeat());
		bookingServiceImpl.bookTicket(ticket);
		model.addObject("ticket", ticket);
		
		return model;
	}

}
