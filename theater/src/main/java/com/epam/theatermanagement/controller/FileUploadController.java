package com.epam.theatermanagement.controller;

import com.epam.theatermanagement.exceptions.ServiceException;
import com.epam.theatermanagement.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class FileUploadController {
	@Autowired
	private FileService fileService;

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ModelAndView showUploadFiles(MultipartHttpServletRequest request) {
		ModelAndView model = new ModelAndView("showUpload");

		Map<String, MultipartFile> fileMap = request.getFileMap();

		if (!fileMap.isEmpty()) {
			try {
				List<String> dataList = fileService.parse(fileMap);
				model.addObject("data", dataList);
			} catch (IOException | ServiceException e) {
				model.addObject("ex", e.getMessage());
				model.setViewName("error");
			}
		}
		return model;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public ModelAndView uploadForm(HttpServletRequest request) {
		return new ModelAndView("upload");
	}
}
