package com.epam.theatermanagement.pdfview;


import com.epam.theatermanagement.domain.Ticket;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class PDFView extends AbstractPdfView {
	
    @SuppressWarnings("unchecked")
	@Override
    protected void buildPdfDocument(Map<String, Object> map, Document document, PdfWriter pdfWriter,
                                    HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws Exception {
		List<Ticket> tickets = (List<Ticket>) map.get("tickets");

        Table table = new Table(1);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        table.addCell("Get Booked Tickets:");

        for (Ticket ticket : tickets) {
            table.addCell(ticket.toString());
        }

        document.add(table);
    }
}
