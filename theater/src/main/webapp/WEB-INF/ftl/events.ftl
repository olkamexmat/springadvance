<a href="${rc.contextPath}/logout">Log Out</a>

<h2>All events</h2>
<table class="datatable">
    <thead>
    <tr>
        <th>Name</th>
        <th>Price</th>
        <th>Rating</th>
        <th>Air dates</th>
        <th></th>
    </tr>
    </thead>
    <#list events as event>
        <tr>
            <td>${event.name}</td>
            <td>${event.basePrice} $</td>
            <td>${event.rating}</td>
            <td>
	            <#list event.airDates as date>
	                ${date}
	            </#list>
            </td>
            <td>
                <input type="button" onclick="location.href='${rc.contextPath}/events/book/${event.id}'" value="Book">
            </td>
            <td>
                <input type="button" onclick="location.href='${rc.contextPath}/events/${event.id}/tickets'" value="Show tickets">
            </td>
            <td>
                <input type="button" onclick="location.href='${rc.contextPath}/events/${event.id}/tickets.pdf'" value="Show tickets in pdf">
            </td>
        </tr>
    </#list>
</table>


