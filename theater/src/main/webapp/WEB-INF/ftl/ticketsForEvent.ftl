<table>
    <tr>
        <th>Date</th>
        <th>Tickets</th>
    </tr>
    <#list tickets as date, ticketsPerDate>
        <tr>
            <td> ${date}</td>
            <td> 
            	<#if ticketsPerDate ??>
            		<#list ticketsPerDate as ticket>
	            		<#if ticket ??>
	            			<p>User : ${ticket.user.firstName} ${ticket.user.lastName}</p>
	            			<p>Events : ${ticket.event.name}</p>
	            			<p>Seat : ${ticket.seat}</p>
	            		<#else> no tickets
	            		</#if>
            		</#list>
            	<#else> no tickets</#if>
            </td>
        </tr>
    </#list>
</table>
