<#import "spring.ftl" as spring />
<html>
	<table>
		<@spring.bind "bookForm"/>	
		<form action="" method="post">	
		    <tr>
		        <th>Event name</th>
		        <th>Base price</th>
		        <th>Rating</th>
		        <th>Choose date for booking</th>
		        <th>Choose auditorium</th>
		        <th>Seat</th>
		    </tr>
		    <tr>
		        <td>${event.name}</td>
		        <td>${event.basePrice} $</td>
		        <td>${event.rating}</td>       
			    <td>
			    	<@spring.bind path="bookForm.date"/>
				    <@spring.formSingleSelect path="bookForm.date" options=dates/>
			    </td>	
			    <td>
			   		<@spring.bind path="bookForm.name"/>
			    	<@spring.formSingleSelect path="bookForm.name" options=auditoriums/>
			    </td>			    
			    <td>
			    	<label>Enter seat within 20</label>
			    	<@spring.bind path="bookForm.seat"/>
			    	<@spring.formInput path="bookForm.seat" fieldType="number"/>
			    </td>
		        <input type="submit" value="book">
		    </tr>
		</form>
	</table>
</html>