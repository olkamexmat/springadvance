<html>
<head>
    <title>Upload Multiple File Request Page</title>
</head>
<body>
<form method="POST" action="${rc.contextPath}/upload" enctype="multipart/form-data">
    <label for="users">Choose file to upload users (in JSON format):</label>
    <input type="file" name="users" id="users">
    <label for="events">Choose file to upload events (in JSON format):</label>
    <input type="file" name="events" id="events">
    <input type="submit" value="Upload"> Press here to upload the file!
</form>
</body>
</html>