<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Login page</title>
</head>
<body>
<div>
<#if error ??>${error}<#else></#if>
</div>
<div>
<#if message ??>${message}<#else></#if>
</div>

<form method="post" action="${rc.contextPath}/login">
    <table>
		<tr>
			<th><label for="email">Email</label></th>
			<td><input name="username" type="text"/></td>
		</tr>
		<tr>
			<th><label for="password">Password</label></th>
			<td><input name="password" type="password"/></td>
		</tr>
		<tr>
			<td>Remember Me: <input type="checkbox" name="remember-me" /></td>
		</tr>
		<tr>
			<th></th>
			<td><input name="commit" type="submit" value="Log In" /></td>
		</tr>
	</table>
</form>
<a href="${rc.contextPath}/events">View events</a>
</body>
</html>